#!/usr/bin/env bash

set -e -x

sudo yum check-update || true

REQUIRED_PKGS="redhat-lsb-core rpm-build vim git cmake wget unzip patch \
devtoolset-2-binutils devtoolset-2-gcc devtoolset-2-gcc-c++ \
popt-devel zlib-devel glib2-devel"

# Check for missing packages
MISSING_PKGS=""
for pkg in $REQUIRED_PKGS; do
    if ! rpm -qi $pkg &> /dev/null; then
        MISSING_PKGS="$MISSING_PKGS $pkg"
    fi
done

if [ "$MISSING_PKGS" ]; then
    sudo yum install -y $MISSING_PKGS
fi

# popt.pc
if [ ! -r /usr/lib64/pkgconfig/popt.pc ]; then
    cat > /usr/lib64/pkgconfig/popt.pc <<EOF
prefix=/usr
exec_prefix=\${prefix}
libdir=/usr/lib64
includedir=\${prefix}/include

Name: popt
Version: 1.13
Description: popt library.
Libs: -L\${libdir} -lpopt
Cflags: -I\${includedir}
EOF
fi
# Google Test

GTEST_VERSION=1.7.0
GTEST_URL=https://googletest.googlecode.com/files/gtest-${GTEST_VERSION}.zip

if [ ! -x /usr/local/src/gtest-${GTEST_VERSION} ]; then
    tmparch=/tmp/arch$$.zip
    wget -qO $tmparch $GTEST_URL && unzip $tmparch -d /usr/local/src
fi
if [ ! -r /etc/profile.d/gtest.sh ]; then
    cat > /etc/profile.d/gtest.sh <<EOF
export GTEST_ROOT=/usr/local/src/gtest-${GTEST_VERSION}
EOF
fi

if [ ! -r /etc/profile.d/devtoolset-2.sh ]; then
    cat > /etc/profile.d/devtoolset-2.sh <<EOF
source /opt/rh/devtoolset-2/enable
EOF
fi

