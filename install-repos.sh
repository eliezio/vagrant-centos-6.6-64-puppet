#!/usr/bin/env bash

set -e -x

if [ ! -r /etc/pki/rpm-gpg/RPM-GPG-KEY-puias ]; then
    rpm --import http://puias.princeton.edu/data/puias/6/x86_64/os/RPM-GPG-KEY-puias
fi

if [ ! -r /etc/yum.repos.d/puias-computational.repo ]; then
    cat > /etc/yum.repos.d/puias-computational.repo <<EOF
[PUIAS_6_computational]
name=PUIAS computational Base \$releasever - \$basearch
mirrorlist=http://puias.math.ias.edu/data/puias/computational/\$releasever/\$basearch/mirrorlist
#baseurl=http://puias.math.ias.edu/data/puias/computational/\$releasever/\$basearch
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-puias
EOF
fi

if [ ! -r /etc/yum.repos.d/puias-devtoolset-2.repo ]; then
    cat > /etc/yum.repos.d/puias-devtoolset-2.repo <<EOF
[DevToolset-2]
name=RedHat DevToolset v2 \$releasever - \$basearch
mirrorlist=http://puias.math.ias.edu/data/puias/DevToolset/\$releasever/\$basearch/mirrorlist
#baseurl=http://puias.princeton.edu/data/puias/DevToolset/\$releasever/\$basearch/
enabled=1
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-puias
EOF
fi

